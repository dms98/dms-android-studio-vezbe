package com.example.vezbe02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private boolean prikazano = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonRegistrujSe = (Button) findViewById(R.id.buttonRegistrujSe);
        buttonRegistrujSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText ime = (EditText) findViewById(R.id.editTextIme);
                EditText prezime = (EditText) findViewById(R.id.editTextPrezime);

                TextView poruka = (TextView) findViewById(R.id.textViewPoruka);
                //String porukica = ime.getText() + " " + prezime.getText();
                if(prikazano)
                {
                    poruka.setText("");
                    prikazano = false;
                }
                else {
                    poruka.setText(ime.getText() + " " + prezime.getText());
                    prikazano = true;
                }
            }
        });
    }
}
