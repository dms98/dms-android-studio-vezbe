package com.example.vezbe041;

import java.util.LinkedList;

public class KontaktApi {
    public static LinkedList<Kontakt> getMyContacts()
    {
        LinkedList<Kontakt> list = new LinkedList<>();

        list.add(new Kontakt("Sima Brat", "+381644923082", Kontakt.TIP_KONTAKTA.PHONE));
        list.add(new Kontakt("Marko Brat", "marko.brat.skype.5", Kontakt.TIP_KONTAKTA.SKYPE));
        list.add(new Kontakt("David Brat", "David.Brat@neveragain.com", Kontakt.TIP_KONTAKTA.EMAIL));

        list.add(new Kontakt("Djes ba", "+381644243082", Kontakt.TIP_KONTAKTA.PHONE));
        list.add(new Kontakt("eii Brat", "eii.Brat.skype.5", Kontakt.TIP_KONTAKTA.SKYPE));
        list.add(new Kontakt("David Davidov", "David.Davidov@neveragain.com", Kontakt.TIP_KONTAKTA.EMAIL));

        list.add(new Kontakt("haha hehe", "+381642323082", Kontakt.TIP_KONTAKTA.PHONE));
        list.add(new Kontakt("hihi hoho", "marko.brat.skype.5", Kontakt.TIP_KONTAKTA.SKYPE));
        list.add(new Kontakt("Dalibor Tot", "David.Brat@neveragain.com", Kontakt.TIP_KONTAKTA.EMAIL));

        list.add(new Kontakt("Milica Tot", "+381334923082", Kontakt.TIP_KONTAKTA.PHONE));
        list.add(new Kontakt("Jovan Jovanovic", "Jovan.Jovanovic.skype.5", Kontakt.TIP_KONTAKTA.SKYPE));
        list.add(new Kontakt("Saban Saulic", "Saban.Saulic@neveragain.com", Kontakt.TIP_KONTAKTA.EMAIL));

        return list;
    }
}
