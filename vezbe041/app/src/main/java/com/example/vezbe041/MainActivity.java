package com.example.vezbe041;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinkedList<Kontakt> list = KontaktApi.getMyContacts();

        LinearLayout mALinearLayout = (LinearLayout) findViewById(R.id.mALinearLayout);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for(Kontakt kontakt : list)
        {
            RelativeLayout item = (RelativeLayout) inflater.inflate(R.layout.my_view, null);
            ((TextView) item.findViewById(R.id.mVTextViewIme)).setText(kontakt.getIme());
            ((TextView) item.findViewById(R.id.mVTextViewVrednost)).setText(kontakt.getVrednost());

            ImageView mVImageView = (ImageView) item.findViewById(R.id.mVImageView);

            switch (kontakt.getTipKontakta())
            {
                case EMAIL:
                    mVImageView.setImageResource(R.drawable.email);
                    break;
                case PHONE:
                    mVImageView.setImageResource(R.drawable.phone);
                    break;
                case SKYPE:
                    mVImageView.setImageResource(R.drawable.skype);
                    break;
            }
            mALinearLayout.addView(item);
        }
    }
}
