package com.example.vezbe6;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Api {
    public static void getJSON(final String url, final ReadDataHandler rdh){
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>(){
            @Override
            protected String doInBackground(String... params) {
                String odgovor = "";

                try {
                    java.net.URL url = new URL(params[0]);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String red;
                    while((red = br.readLine()) != null){
                        odgovor += red + "\n";
                    };
                    br.close();
                    con.disconnect();
                } catch (Exception e){
                    odgovor = "[]";
                }

                return odgovor;
            }

            @Override
            protected void onPostExecute(String odgovor) {
                rdh.setJson(odgovor);
                rdh.sendEmptyMessage(0);
            }
        };
        task.execute(url);
    }
}
