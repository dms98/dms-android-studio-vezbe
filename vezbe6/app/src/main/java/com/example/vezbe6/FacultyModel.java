package com.example.vezbe6;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FacultyModel {
    String name, acronym, website;

    public FacultyModel() {

    }

    public FacultyModel(String name, String acronym, String website) {
        this.name = name;
        this.acronym = acronym;
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getWebsite() {
        return website;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public static FacultyModel parseJSONObject(JSONObject object){
        FacultyModel faculty = new FacultyModel();

        try {
            if (object.has("name")) {
                faculty.setName(object.getString("name"));
            }

            if (object.has("acronym")) {
                faculty.setAcronym(object.getString("acronym"));
            }

            if (object.has("website")) {
                faculty.setWebsite(object.getString("website"));
            }

        } catch (Exception e){

        }

        return  faculty;
    }

    public static ArrayList<FacultyModel> parseJSONArray(JSONArray array){
        ArrayList<FacultyModel> list = new ArrayList<FacultyModel>();

        try{
            for(int i = 0; i < array.length(); i++){
//                FacultyModel facultyModel = parseJSONObject(array.getJSONObject(i));
                list.add(parseJSONObject(array.getJSONObject(i)));
            }
        } catch (Exception e){

        }

        return  list;
    }
}
