package com.example.vezbe6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Message;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initFaculties();
    }

    public void initFaculties(){
        Api.getJSON("http://199.188.100.46/singidunum/android/faculties.json", new ReadDataHandler(){
            @Override
            public void handleMessage(Message msg) {
                try {
                    String odgovor = getJson();
                    JSONArray array = new JSONArray(odgovor);
                    ArrayList<FacultyModel> faculties = FacultyModel.parseJSONArray(array);

                    TextView aMTextViewJSON = (TextView) findViewById(R.id.aMTextViewJSON);
                    aMTextViewJSON.setText("Fakulteti:\n\n");

                    if(faculties.size() > 0){
                        for(FacultyModel faculty:faculties){
                            String prikazFakulteta = "[ " + faculty.getAcronym() + " ] " + faculty.getName() +  "\n" + faculty.getWebsite() + "\n\n";
                            aMTextViewJSON.append(prikazFakulteta);
                        }
                    } else {
                        aMTextViewJSON.setText("Nema fakulteta!");
                    }
                } catch (Exception e){

                }
            }
        });
        ((TextView) findViewById(R.id.aMTextViewJSON)).setText("Ucitava se...");
    }
}
