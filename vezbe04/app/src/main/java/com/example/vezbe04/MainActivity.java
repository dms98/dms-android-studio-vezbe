package com.example.vezbe04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.mAButtonRegistrujSe).setOnClickListener(this);
        findViewById(R.id.mAButtonOdustani).setOnClickListener(this);

    }
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.mAButtonOdustani:
                this.doOdustani();
                break;
            case R.id.mAButtonRegistrujSe:
                this.doRegistrujSe();
                break;
        }
    }
    private void doRegistrujSe()
    {
        String ime = ((EditText) findViewById(R.id.mAEditTextIme)).getText().toString();
        String prezime = ((EditText) findViewById(R.id.mAEditTextPrezime)).getText().toString();
        if(ime.isEmpty() || prezime.isEmpty())
        {
            ((TextView)findViewById(R.id.mATextViewPoruka)).setText("Popuniti sva polja!");
        }
        else
        {
            Intent i = new Intent(this, Detalji.class);
            startActivity(i);
        }
    }
    private void doOdustani()
    {
        finish();
    }
}
